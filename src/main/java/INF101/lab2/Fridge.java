package INF101.lab2;

import java.util.List;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    public ArrayList<FridgeItem> fridgeItems = new ArrayList<FridgeItem>();
    public int maxItems = 20;

    public Fridge() {
        fridgeItems = new ArrayList<FridgeItem>();
    }

    public int nItemsInFridge() {
        return fridgeItems.size();
    }

    public int totalSize() {
        return maxItems;
    }

    public boolean placeIn(FridgeItem item) {
        if(nItemsInFridge() >= maxItems) {
            return false;
        }
        else {
            return fridgeItems.add(item);
        }
    }

    public void takeOut(FridgeItem item) {
        if (fridgeItems.contains(item)) {
            fridgeItems.remove(item);
        }
        else {
            throw new NoSuchElementException("Item not in fridge");
        }
    }

    public void emptyFridge() {
        fridgeItems.clear();
    }

    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();
        for(FridgeItem item : fridgeItems){
            if(item.hasExpired()) {
                expiredItems.add(item);
            }
        }
        for(FridgeItem expiredItem : expiredItems){
            fridgeItems.remove(expiredItem);
        }
        return expiredItems; 
    }
}
